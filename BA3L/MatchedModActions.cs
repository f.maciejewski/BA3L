﻿using System;
using System.Collections.Generic;

namespace BA3L.ModActions
{
	public abstract class MatchedModAction
	{
		public abstract override string ToString();
	}

	public class KeepEnabled : MatchedModAction
	{
		public Mod ModToKeep { get; private set; }
		public KeepEnabled(Mod mod) { ModToKeep = mod; }
		public override string ToString()
		{
			return $"Keep enabled: {ModToKeep.Name}";
		}
	}

	public class KeepDisabled : MatchedModAction
	{
		public Mod ModToKeep { get; private set; }
		public KeepDisabled(Mod mod) { ModToKeep = mod; }

		public override string ToString()
		{
			return $"Keep disabled: {ModToKeep.Name}";
		}
	}

	public class EnableMod : MatchedModAction
	{
		public Mod ModToEnable { get; private set; }
		public EnableMod(Mod mod) { ModToEnable = mod; }

		public override string ToString()
		{
			return $"Enable mod: {ModToEnable.Name}";
		}
	}

	public class DisableMod : MatchedModAction
	{
		public Mod ModToDisable { get; private set; }
		public DisableMod(Mod mod) { ModToDisable = mod; }

		public override string ToString()
		{
			return $"Disable mod: {ModToDisable.Name}";
		}
	}

	public class SubscribeToWorkshopMod : MatchedModAction // Change to SubscribeAndEnable?
	{
		public ulong WorkshopId { get; private set; }
		public string Name { get; private set; }
		public SubscribeToWorkshopMod(ulong id, string name) { WorkshopId = id; Name = name; }

		public override string ToString()
		{
			return $"Subscribe: {Name}";
		}
	}

	public class DoNothing : MatchedModAction
	{
		public override string ToString()
		{
			return "Do nothing";
		}
	}
}

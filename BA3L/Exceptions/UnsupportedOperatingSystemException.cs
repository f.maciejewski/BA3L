﻿using System;
namespace BA3L.Exceptions
{
	public class UnsupportedOperatingSystemException: Exception
	{
		public UnsupportedOperatingSystemException() {}
		public UnsupportedOperatingSystemException(string message) : base(message) {}
	}
}

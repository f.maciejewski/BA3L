﻿using System;
using Steamworks;
using Steamworks.Data;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace BA3L
{
	// TODO make some args
	public class OnServerRespondEventArgs : EventArgs
	{
		public readonly Server RespondedServer;

		public OnServerRespondEventArgs(Server server)
		{
			RespondedServer = server;
		}
	} 
	public class OnServerRefreshCompleteEventArgs : EventArgs { }

	public enum ServerQueryType
	{
		Unknown,
		Internet,
		Favourites,
		History,
		LAN,
	}

	public static class ServerBrowser
	{
		public static event EventHandler<OnServerRespondEventArgs> OnInternetServerRespondEvent;
		public static event EventHandler<OnServerRespondEventArgs> OnOtherServerRespondEvent;
		public static event EventHandler<OnServerRefreshCompleteEventArgs> OnServerRefreshCompleteEvent;

		private static Steamworks.ServerList.Base CurrentQuery;

		// Types of query we might want to deal with:
		// Favourites
		// Friends
		// History/
		// Internet
		// LAN
		// Official (Arma 3's browser has this, but isn't recognized by the Steam API, so probably uses a tag)
		//		Might be a pre-defined list of IP addresses actually.

		private static List<Server> InternetServerList = new List<Server>(); // For bigger Internet queries
		private static List<Server> OtherServerList = new List<Server>(); // Smaller queries like Friends, History, etc.

		public const int DefaultTimeoutSeconds = 30;

		// These should probably be properties
		public static ReadOnlyCollection<Server> GetInternetServerList()
		{
			return new ReadOnlyCollection<Server>(InternetServerList);
		}

		public static ReadOnlyCollection<Server> GetOtherServerList()
		{
			return new ReadOnlyCollection<Server>(OtherServerList);
		}

		private static async Task RefreshServersAsync(Steamworks.ServerList.Base newQuery, 
			List<Server> listToClear, Action<ServerInfo> OnResponsive)
		{
			DisposeCurrentQuery();

			// Clear the list of servers for this query type
			listToClear.Clear();

			CurrentQuery = newQuery;

			// Get responsive servers as they come instead of waiting.
			CurrentQuery.OnResponsiveServer += OnResponsive;

			ServerFilters.AddFilters(CurrentQuery); // TODO consider not filtering Friends apart from AppId

			// Actually do the query
			// As far as I know, the speed of the queries is limited by Steam's settings.
			// You can change this in Steam -> Settings -> In-Game -> "In-Game server browser: Max pings/minute"
			bool queryComplete = await CurrentQuery.RunQueryAsync(DefaultTimeoutSeconds);

			// Will be false if we cancel the query
			if(queryComplete)
			{
				// Query is done now
				OnServerRefreshCompleteEvent?.Invoke(null, new OnServerRefreshCompleteEventArgs());
			}
		}

		public static void RefreshInternetServers()
		{
			Task.Run(() => RefreshServersAsync(new Steamworks.ServerList.Internet(),
				InternetServerList, OnResponsiveInternetServer)).ConfigureAwait(false);
		}

		public static void RefreshFavouriteServers()
		{
			Task.Run(() => RefreshServersAsync(new Steamworks.ServerList.Favourites(),
				OtherServerList, OnResponsiveOtherServer)).ConfigureAwait(false);
		}

		public static void RefreshFriendsServers()
		{
			Task.Run(() => RefreshServersAsync(new Steamworks.ServerList.Friends(),
				OtherServerList, OnResponsiveOtherServer)).ConfigureAwait(false);
		}

		public static void RefreshLANServers()
		{
			Task.Run(() => RefreshServersAsync(new Steamworks.ServerList.LocalNetwork(),
				OtherServerList, OnResponsiveOtherServer)).ConfigureAwait(false);
		}

		public static void RefreshRecentServers()
		{
			// Seems to be a bit slow to refresh but idk, might be trying to get non-Arma servers
			Task.Run(() => RefreshServersAsync(new Steamworks.ServerList.History(),
				OtherServerList, OnResponsiveOtherServer)).ConfigureAwait(false);
		}

		private static void OnResponsiveInternetServer(ServerInfo responsiveServer)
		{
			// Only filter by appid here
			// More consistent if we only filter based on the last refresh
			// Let the UI handle filtering once the refresh has started.
			if(responsiveServer.AppId != Utilities.ARMA3_APPID)
				return;

			Server s = new Server(responsiveServer);
			InternetServerList.Add(new Server(responsiveServer));
			OnInternetServerRespondEvent?.Invoke(null, new OnServerRespondEventArgs(s));
		}

		private static void OnResponsiveOtherServer(ServerInfo responsiveServer)
		{
			if(responsiveServer.AppId != Utilities.ARMA3_APPID)
				return;

			Server s = new Server(responsiveServer);
			OtherServerList.Add(s);
			OnOtherServerRespondEvent?.Invoke(null, new OnServerRespondEventArgs(s));
		}

		public static void DisposeCurrentQuery()
		{
			if(CurrentQuery != null)
			{
				// Does this crash if we -= an event that doesn't exist?
				CurrentQuery.OnResponsiveServer -= OnResponsiveInternetServer;
				CurrentQuery.OnResponsiveServer -= OnResponsiveOtherServer;

				CurrentQuery.Dispose();
				CurrentQuery = null;
			}
		}


	}
}

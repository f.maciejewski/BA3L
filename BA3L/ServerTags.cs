﻿using System;
using Steamworks.Data;
using System.Collections.Generic;


namespace BA3L
{
	// https://community.bistudio.com/wiki/STEAMWORKSquery#Server_State
	public enum ServerState : int
	{
		None = 0,
		Selecting = 1,
		Editing = 2,
		Assigning = 3,
		Sending = 4,
		Loading = 5,
		Briefing = 6,
		Playing = 7,
		Debriefing = 8,
		Aborted = 9,
	};

	public enum DifficultyTag : int
	{
		Unknown = -1,
		Recruit = 0,
		Regular = 1,
		Veteran = 2,
		Custom = 3
	};


	/// <summary>
	/// Decodes and exposes the values of the available Arma 3 tags for its server.
	/// https://community.bistudio.com/wiki/STEAMWORKSquery#SetGameTags_in_detail
	/// </summary>
	public class ServerTags
	{
		private Dictionary<string, string> TagDictionary = new Dictionary<string, string>();

		public bool BattlEye { get; private set; }
		public string RequiredVersion { get; private set; }
		public string RequiredBuildNo { get; private set; }
		public ServerState State { get; private set; }
		public DifficultyTag Difficulty { get; private set; }
		public bool EqualModRequired { get; private set; }
		public bool Lock { get; private set; }
		public bool VerifySignatures { get; private set; }
		public bool Dedicated { get; private set; }
		public Utilities.OS Platform { get; private set; }
		public string LoadedContentHash { get; private set; }
		public int TimeLeftMinutes { get; private set; }
		public bool AllowedFilePatching { get; private set; }

		public ServerTags(ServerInfo server)
		{
			try
			{
				ExtractTags(server);
				DecodeTags();
			}
			catch(Exception e)
			{
				Console.Error.WriteLine($"Server tags for server {server.Address} failed:\n{e.Message}\n{e.StackTrace}");
			}
		}

		/// <summary>
		/// Separates the identifiers and the values from the tags and put them into a dictionary for easier use.
		/// </summary>
		/// <param name="server">Server.</param>
		private void ExtractTags(ServerInfo server)
		{
			string[] tags = server.Tags;

			foreach(string tag in tags)
			{
				if(tag.Length < 2)
					continue;
				string key = tag[0].ToString();
				string value = tag.Substring(1);
				TagDictionary.Add(key, value);
			}
		}

		private void DecodeTags()
		{
			BattlEye = GetBoolValue("b");

			// RequiredVersion
			if(TagDictionary.TryGetValue("r", out string RequiredVersionValue))
			{
				RequiredVersion = RequiredVersionValue.Insert(1, ".");
			}

			RequiredBuildNo = GetStringValue("n", "-1");

			// ServerState
			// https://community.bistudio.com/wiki/STEAMWORKSquery#Server_State
			if(TagDictionary.TryGetValue("s", out string ServerStateValue))
			{
				if(int.TryParse(ServerStateValue, out int result))
				{
					State = (ServerState)result;
				}
			}

			// Difficulty (Probably Regular, Veteran, Custom)
			// Won't be as detailled as the rules query.
			if(TagDictionary.TryGetValue("i", out string DifficultyValue))
			{
				// i-1 is Unknown (Just no value in the server browser)
				// i1 = Regular
				// i2 = Veteran
				// i3 = Custom
				if(int.TryParse(DifficultyValue, out int result))
				{
					if(result < (int)DifficultyTag.Unknown || result > (int)DifficultyTag.Veteran)
					{
						Difficulty = DifficultyTag.Unknown;
					}
					else
						Difficulty = (DifficultyTag)result;
				}
				else
				{
					Difficulty = DifficultyTag.Unknown;
				}
			}

			EqualModRequired = GetBoolValue("m");

			// Is the server "locked"? Different from having a password.
			Lock = GetBoolValue("l");

			VerifySignatures = GetBoolValue("v");

			Dedicated = GetBoolValue("d");

			// TODO GameType
			// Get string value then compare it to a dictionary of known game types
			// https://community.bistudio.com/wiki/STEAMWORKSquery#Game_Type

			// TODO longitude and latitude (LongLat)
			// Noticed that servers often give bogus data (lowest int value)

			// Platform
			string platformString = GetStringValue("p");
			if(platformString == "w")
				Platform = Utilities.OS.OS_WINDOWS;
			else if(platformString == "l")
				Platform = Utilities.OS.OS_LINUX;
			else
				Platform = Utilities.OS.OS_UNKNOWN;

			LoadedContentHash = GetStringValue("h");

			// TODO country

			// Time left
			if(TagDictionary.TryGetValue("e", out string TimeLeftValue))
			{
				if(int.TryParse(TimeLeftValue, out int result))
				{
					TimeLeftMinutes = result;
				}
				else
				{
					TimeLeftMinutes = -1;
				}
			}
			else
			{
				TimeLeftMinutes = -1;
			}

			// Apparently mission-specific parameters
			// https://community.bistudio.com/wiki/Description.ext#Mission_parameters
			// TODO figure out what param1 "j" is
			// TODO figure out what param2 "k" is

			// allowedFilePatching
			AllowedFilePatching = GetBoolValue("f");

			// The doc mentions the "y" "island?" flag but doesn't seem to be implemented
		}

		private bool GetBoolValue(string tagType, bool defaultValue = false)
		{
			if(TagDictionary.TryGetValue(tagType, out string result))
			{
				return result == "t" || result == "1";
			}
			else
			{
				return defaultValue;
			}
		}

		private string GetStringValue(string tagType, string defaultValue = "")
		{
			if(TagDictionary.TryGetValue(tagType, out string result))
			{
				return result;
			}
			else
			{
				return defaultValue;
			}
		}
	}
}

﻿using System;
using Steamworks.Data;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace BA3L
{
	public enum RulesQueryInfo
	{
		Unknown,
		Querying,
		Success,
		Failed,
	}

	public class Server
	{
		private ServerInfo serverInfo;
		public ServerRules Rules { get; private set; }
		public RulesQueryInfo RulesQueryStatus = RulesQueryInfo.Unknown;
		public ServerTags Tags { get; private set; }

		// Server names seem to be limited to 64 characters. Might be a Steamworks limitation.
		public string Name => serverInfo.Name;
		public System.Net.IPAddress Address => serverInfo.Address;
		public int ConnectionPort => serverInfo.ConnectionPort;
		public int Ping => serverInfo.Ping; // Obviously milliseconds.
		public int Players => serverInfo.Players;
		public int MaxPlayers => serverInfo.MaxPlayers;
		public bool Passworded => serverInfo.Passworded;
		public string Map => serverInfo.Map;
		public string Mission => serverInfo.Description;

		// Version is probably also in the tags which we could've looked at.
		public int Build => serverInfo.Version; // Full build ID as int e.g. 194145977 which is 1.94.145977
		public string Version => Build.ToString().Substring(0, 3).Insert(1, "."); // Major.Minor version string
		public string FullVersion => Build.ToString().Insert(1, ".").Insert(4, "."); // Full build ID as string



		public Server(ServerInfo newServerInfo)
		{
			serverInfo = newServerInfo;
			GetRules();
			Tags = new ServerTags(serverInfo);
		}

		public void GetRules()
		{
			RulesQueryStatus = RulesQueryInfo.Querying;
			Task.Run(GetRulesAsync).ConfigureAwait(false);
		}

		public async Task GetRulesAsync()
		{
			Dictionary<byte[], byte[]> rulesDictionary = await ServerQueries.GetRules(serverInfo);

			try
			{
				ServerRules newRules = new ServerRules(rulesDictionary);
				Rules = newRules;
				RulesQueryStatus = RulesQueryInfo.Success;
			}
			catch(Exceptions.InvalidRulesException)
			{
				// TODO include fail reason?

				// Possible reasons so far:
				// First byte of A2S_RULES response was not 0x45 ('E')
				// Server Brower Protocol message was not version 3 (May backport version 2 later idk)
				// Crosshair byte was not a 0 or 1.
				// Unexpected end of stream reached.
				RulesQueryStatus = RulesQueryInfo.Failed;
			}
			catch(System.IO.EndOfStreamException)
			{
				// Expecting this just in case.
				// Might happen if the query overflowed but we didn't account for it properly.
				RulesQueryStatus = RulesQueryInfo.Failed;
			}
		}
	}
}

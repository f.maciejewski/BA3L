﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;


namespace BA3L
{
	public class Mod
	{
		public ulong WorkshopId { get; private set; }   // Identifier for workshop enable/disable
		private Steamworks.Ugc.Item workshopItem;

		private string ModName = "";

		private List<string> Keys = new List<string>();

		// Name of last folder, identifier for enable/disable
		public string ModFolderName
		{
			get
			{
				if(UsesSteamWorkshop())
				{
					return WorkshopId.ToString();
				}
				else
				{
					return new DirectoryInfo(nonWorkshopFolderPath).Name;
				}
			}
		}

		public bool Enabled { get; private set; }

		public string Identifier
		{
			get
			{
				if(UsesSteamWorkshop())
				{
					return WorkshopId.ToString();
				}
				else
				{
					return ModFolderName;
				}
			}
		}

		public string Name
		{
			get
			{
				if(ModName != "")
					return ModName;
				else return ModFolderName;
			}
		}

		// Workshop state flags
		public bool IsInstalled => workshopItem.IsInstalled;
		public bool IsDownloading => workshopItem.IsDownloading;
		public bool IsDownloadPending => workshopItem.IsDownloadPending;
		public bool IsSubscribed => workshopItem.IsSubscribed;
		public bool NeedsUpdate => workshopItem.NeedsUpdate;
		public bool IsReadyToPlay => !UsesSteamWorkshop() || (IsInstalled && !IsDownloading && !IsDownloadPending && !NeedsUpdate);

		// Consider doing this better.
		public string Status
		{
			get
			{
				if(IsReadyToPlay)
					return "Ready";
				else if(IsDownloading)
					return "Downloading";
				else if(IsDownloadPending)
					return "Download pending";
				else if(NeedsUpdate)
					return "Needs update";
				else if(IsInstalled)
					return "Installed";
				else
					return "Not installed";
			}
		}


		private readonly string nonWorkshopFolderPath;

		public Mod(string modDirectory)
		{
			// Assume modDirectory is full path
			nonWorkshopFolderPath = modDirectory;

			// TODO look for mod name in meta.cpp if we don't find it in mod.cpp
			// TODO again maybe doing both is helpful (even for Workshop mods)

			string modCppPath = Path.Combine(nonWorkshopFolderPath, "mod.cpp");
			if(File.Exists(modCppPath))
			{
				List<KeyValuePair<string, string>> keyValuePairs = ReadModCpp(modCppPath);

				KeyValuePair<string, string> namePair = FindKeyValuePair("name", keyValuePairs);
				if(!namePair.Equals(default(KeyValuePair<string, string>)))
				{
					ModName = namePair.Value;
				}
			}

			FindModSignatures();
		}

		public Mod(Steamworks.Ugc.Item item)
		{
			workshopItem = item;
			WorkshopId = workshopItem.Id;
			ModName = workshopItem.Title;
			FindModSignatures();
		}

		public List<KeyValuePair<string, string>> ReadModCpp(string modCppPath)
		{
			List<KeyValuePair<string, string>> keyValuePairs = new List<KeyValuePair<string, string>>();
			// Assume file exists
			string modCppData = File.ReadAllText(modCppPath);

			// Cover both Windows and Unix line ending forms.
			char[] separators = { '\r', '\n' };
			string[] lines = modCppData.Split(separators, StringSplitOptions.RemoveEmptyEntries);

			// Key, optional whitespace, =, optional whitespace, value, semicolon
			Regex modRegex = new Regex(@"(?<key>\w+)\s*=\s*(?<value>([^;]*))");

			foreach(string line in lines)
			{
				Match keyValueMatch = modRegex.Match(line);
				if(!keyValueMatch.Success)
					continue;

				string key = keyValueMatch.Groups["key"].Value;
				string value = keyValueMatch.Groups["value"].Value;
				value = value.Trim('"');
				KeyValuePair<string, string> pair = new KeyValuePair<string, string>(key, value);
				keyValuePairs.Add(pair);
			}

			return keyValuePairs;
		}

		public KeyValuePair<string, string> FindKeyValuePair(string keyName, List<KeyValuePair<string, string>> keyValuePairs)
		{
			foreach(KeyValuePair<string, string> pair in keyValuePairs)
			{
				if(pair.Key == keyName)
				{
					return pair;
				}
			}

			return default(KeyValuePair<string, string>);
		}

		private void FindModSignatures()
		{
			if(!IsReadyToPlay)
				return; // TODO wait until ready to play to perform this.

			// Look through the Keys or keys, etc. folders
			// I'm using Keys and Signatures interchangably.
			// This might result in empty signatures if a Workshop mod is downloading when this function is called.
			List<string> keyList = new List<string>();
			string path = GetPath();
			if(path != null)
			{
				var directories = Directory.EnumerateDirectories(path);
				List<string> keyDirectories = new List<string>();
				foreach(string directory in directories)
				{
					// Silly workaround for some mods having "keys" or "Keys" or something else.
					string lastFolderName = new DirectoryInfo(@directory.TrimEnd(Path.DirectorySeparatorChar)).Name;
					if(lastFolderName.Equals("keys", StringComparison.OrdinalIgnoreCase))
					{
						keyDirectories.Add(directory);
					}
				}

				foreach(string keyDir in keyDirectories)
				{
					var keyFiles = Directory.EnumerateFiles(keyDir);
					foreach(string keyFile in keyFiles)
					{
						string lastName = Path.GetFileName(keyFile);
						if(lastName.EndsWith(".bikey", StringComparison.OrdinalIgnoreCase))
						{
							lastName = lastName.Replace(".bikey", "");
							keyList.Add(lastName);
						}
					}
				}
			}

			Keys = keyList;
		}

		public bool HasSignature(string signature)
		{
			return Keys.Any(s => signature.StartsWith(s, StringComparison.InvariantCulture));
		}

		// Some of these could be made into properties.
		public bool UsesSteamWorkshop()
		{
			return WorkshopId > 0uL;
		}

		public string GetPath()
		{
			if(UsesSteamWorkshop())
			{
				if(IsInstalled)
				{
					return workshopItem.Directory;
				}
				else
				{
					return null;
				}
			}
			else
			{
				return nonWorkshopFolderPath;
			}
		}

		/// <summary>
		/// Gets the path of game relative to Wine/Proton's "Z:\" drive.
		/// </summary>
		/// <returns>The mod's path as if it were running through Proton.</returns>
		public string GetProtonPath()
		{
			const string protonDisk = "Z:";
			return protonDisk + GetPath().Replace("/", "\\\\"); // Apparently
		}

		/// <summary>
		/// Set this mod as enabled. Only call from ModManager.EnableMod().
		/// </summary>
		public void Enable()
		{
			Enabled = true;
		}

		/// <summary>
		/// Set this mod as disabled. Only call from ModManager.DisableMod().
		/// </summary>
		public void Disable()
		{
			Enabled = false;
		}
	}
}

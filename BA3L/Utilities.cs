﻿using System;
using System.Runtime.InteropServices;
using System.IO;
using System.Diagnostics;
using Steamworks;

namespace BA3L
{
	public static class Utilities
	{
		public const int ARMA3_APPID = 107410;

		public static string ArgsToString(string[] args)
		{
			string argsString = "";

			foreach (string arg in args)
			{
				argsString += arg + " ";
			}

			return argsString;
		}

		public enum OS
		{
			OS_UNKNOWN,
			OS_WINDOWS,
			OS_OSX,
			OS_LINUX
		};

		public static OS GetOS()
		{
			// Passing this here in case I decide to not use IsOSPlatform
			// Which only exists in .NET 4.7.1 (October 2017) or newer
			// I probably will keep using IsOSPlatform so this may need refactoring.
			if(RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
			{
				return OS.OS_WINDOWS;
			}
			else if(RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
			{
				return OS.OS_OSX;
			}
			else if(RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
			{
				return OS.OS_LINUX;
			}
			else
			{
				return OS.OS_UNKNOWN;
			}
		}

		public static string GetArma3Path()
		{
			if(SteamClient.IsValid)
			{
				// TODO consider doing something if this is null
				return SteamApps.AppInstallDir(ARMA3_APPID);
			}
			else
			{
				// We can't really assume it's installed in the default location
				// If Windows:
				if (GetOS() == OS.OS_WINDOWS)
				{
					// Windows can use a registry entry
					string registryPath;
					if (Environment.Is64BitOperatingSystem)
					{
						registryPath = "HKEY_LOCAL_MACHINE\\Software\\Wow6432Node\\bohemia interactive\\arma 3";
					}
					else
					{
						registryPath = "HKEY_LOCAL_MACHINE\\Software\\bohemia interactive\\arma 3";
					}

					string arma3Path = Microsoft.Win32.Registry.GetValue(registryPath, "main", "").ToString();
					if (arma3Path != "")
					{
						return arma3Path;
					}
					else
					{
						throw new DirectoryNotFoundException("Unable to find Arma 3 path. Ensure it has been run once.");
					}
				}
				else if (GetOS() == OS.OS_LINUX)
				{
					// Search steamapps directories for Arma 3
					// The manifest file will exist in the appropriate directory: appmanifest_107410.acf
					// Other steamapps can be found in ~/.local/share/Steam/config/config.vdf
					// Flatpak puts Steam install elsewhere, though.
					// TODO add support for alternate Steam directories, Flatpak, etc.

					// For now assume the default location is used
					string homeDir = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
					string arma3Path = Path.Combine(homeDir, ".local/share/Steam/steamapps/common/Arma 3/");
					if (Directory.Exists(arma3Path))
					{
						return arma3Path;
					}
					else
					{
						throw new DirectoryNotFoundException("Unable to find Arma 3 path. The search needs to be done properly.");
					}
				}
				else if (GetOS() == OS.OS_OSX)
				{
					// MacOS not yet supported
					throw new Exceptions.UnsupportedOperatingSystemException("MacOS not supported yet");
				}
				else
				{
					throw new Exceptions.UnsupportedOperatingSystemException("Unknown Operating System!");
				}

			}
		}

		/// <summary>
		/// Determines whether the install of Arma 3 is using Proton.
		/// (If arma3launcher.exe exists and the host OS is Linux, we say it is Proton.)
		/// </summary>
		/// <returns><c>true</c>, if the install uses Proton, <c>false</c> otherwise.</returns>
		public static bool IsProton()
		{
			// This is another method that could have been a property.
			string gamePath = GetArma3Path();
			string launcherPath = Path.Combine(gamePath, "arma3launcher.exe");
			return File.Exists(launcherPath) && (GetOS() == OS.OS_LINUX);
		}

		// TODO a proper version?
		public static string GetGameVersion()
		{
			if(GetOS() == OS.OS_WINDOWS || IsProton())
			{
				string gamePath = GetArma3Path();
				string exeName = "arma3.exe";
				string gameexePath = Path.Combine(gamePath, exeName);
				FileVersionInfo versionInfo = FileVersionInfo.GetVersionInfo(gameexePath);
				return $"{versionInfo.ProductMajorPart}.{versionInfo.ProductMinorPart}";
			}
			else
			{
				return "1.82"; // TODO What is this on the native build?
			}
		}
	}
}

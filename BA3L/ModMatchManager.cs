﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace BA3L
{
	public static class ModMatchManager
	{

		// TODO should the public method just have a Server argument?
		public static List<MatchedMod> MatchServerMods(ReadOnlyCollection<ServerMod> serverMods,
			ReadOnlyCollection<string> signatures)
		{
			List<MatchedMod> matchedMods = new List<MatchedMod>();

			List<ServerMod> validServerMods = RemoveIgnoredMods(serverMods);
			List<string> validSignatures = RemoveIgnoredSignatures(signatures);

			List<Mod> clientModsAlreadyFound = new List<Mod>(); // Helpful to avoid duplicating mods when searching signatures

			foreach(ServerMod serverMod in validServerMods)
			{
				// If it's a Workshop mod, we can look here.
				if(serverMod.WorkshopId > 0uL)
				{
					if(ModManager.FindModByWorkshopId(serverMod.WorkshopId, out Mod foundMod))
					{
						// We have the Workshop mod installed
						MatchedMod matched = new MatchedMod(foundMod.Name);
						matched.ServerMod = serverMod;
						matched.WasMatched = true;
						matched.MatchType = ModMatchType.Modlist;

						if(foundMod.Enabled)
						{
							// If it's enabled, use a KeepEnabled action
							matched.AddAction(new ModActions.KeepEnabled(foundMod));
							matched.AddAction(new ModActions.DisableMod(foundMod));
						}
						else
						{
							// If it's disabled, use an EnableMod action
							matched.AddAction(new ModActions.EnableMod(foundMod));
							matched.AddAction(new ModActions.KeepDisabled(foundMod));
						}

						if(!clientModsAlreadyFound.Contains(foundMod))
						{
							clientModsAlreadyFound.Add(foundMod);
						}

						matchedMods.Add(matched);
						continue;
					}
					else
					{
						// A workshop mod, but we don't have it installed
						MatchedMod matched = new MatchedMod(serverMod.Name);
						matched.ServerMod = serverMod;
						matched.WasMatched = true; // We found it, it's just not installed
						matched.MatchType = ModMatchType.Modlist;
						matched.AddAction(new ModActions.SubscribeToWorkshopMod(serverMod.WorkshopId, matched.ServerMod.Name));
						matched.AddAction(new ModActions.DoNothing());
						matchedMods.Add(matched);
						continue;
					}
				}
				else // Not found on the Workshop
				{
					// TODO
					// Map it to a Workshop mod?
					// Map it to hash but we don't have hashes for installed mods yet

					// Map by name
					// TODO we may be able to increase accuracy if we look in mod's meta.cpp and mod.cpp for names
					// TODO or even look through a list of known mods that matches common mod names to Workshop items
					List<Mod> foundNames = ModManager.FindModsByName(serverMod.Name);

					if(!foundNames.Any() && serverMod.Name.StartsWith("@", StringComparison.InvariantCulture))
					{
						foundNames = ModManager.FindModsByName(serverMod.Name.TrimStart('@'));
					}

					if(foundNames.Any())
					{
						MatchedMod matched = new MatchedMod(foundNames[0].Name);
						matched.ServerMod = serverMod;
						matched.WasMatched = true;
						matched.MatchType = ModMatchType.Modlist;

						foreach(Mod mod in foundNames)
						{
							// We might be mapping one name to multiple mods
							// Might mean that we'll end up with multiple entries for the same mod
							// Instead of having one entry with options for each matched mod
							if(!clientModsAlreadyFound.Contains(mod))
							{
								clientModsAlreadyFound.Add(mod);
							}
							if(mod.Enabled)
							{
								matched.AddAction(new ModActions.KeepEnabled(mod));
								matched.AddAction(new ModActions.DisableMod(mod));
							}
							else
							{
								matched.AddAction(new ModActions.EnableMod(mod));
								matched.AddAction(new ModActions.KeepDisabled(mod));
							}
						}

						matchedMods.Add(matched);
					}
					else
					{
						// No mods found bearing the name
						// TODO more such as trying to map it to a Workshop mod
						Console.WriteLine($"No match found for mod {serverMod.Name}");
					}
				}
			}


			// Note when I map signatures to mods:
			// It's possible one signature can be matched to multiple mods
			// We should give the opportunity to toggle each matched mod separately
			// In case one key is used across multiple mods
			// E.g. Blastcore Edited (standalone version) and Vanilla smoke (for Blastcore Edited)
			// Both share the same key ruPal_mpkey.bikey
			// But clearly I might want both, or only one, or neither.

			foreach(string signature in validSignatures)
			{
				List<Mod> modsMatchingSignature = ModManager.FindModsWithSignature(signature);

				// If the mod hasn't already been listed, put it in.
				foreach(Mod mod in modsMatchingSignature)
				{
					if(!clientModsAlreadyFound.Contains(mod))
					{
						MatchedMod matched = new MatchedMod(mod.Name);
						matched.MatchType = ModMatchType.Signature;
						matched.WasMatched = true;
						// Matched to a signature rather than a ServerMod
						// TODO maybe some dependency handling.
						if(mod.Enabled)
						{
							matched.AddAction(new ModActions.KeepEnabled(mod));
							matched.AddAction(new ModActions.DisableMod(mod));
						}
						else
						{
							matched.AddAction(new ModActions.EnableMod(mod));
							matched.AddAction(new ModActions.KeepDisabled(mod));
						}

						matchedMods.Add(matched);
						clientModsAlreadyFound.Add(mod);
					}
				}


			}

			// What about mods that are enabled but aren't allowed?
			// Essentially every enabled mod not yet in clientModsAlreadyFound.
			// ModManager.GetEnabledMods() then either a foreach loop or a linq query against clientModsAlreadyFound
			foreach(Mod enabledMod in ModManager.GetEnabledMods())
			{
				if(!clientModsAlreadyFound.Contains(enabledMod))
				{
					// If the enabled mod isn't in our list of already-found mods
					// It is probably incompatible with the server

					MatchedMod unMatched = new MatchedMod(enabledMod.Name);
					unMatched.MatchType = ModMatchType.NotAllowed;
					unMatched.AddAction(new ModActions.DisableMod(enabledMod));
					unMatched.AddAction(new ModActions.KeepEnabled(enabledMod));
					matchedMods.Add(unMatched);
				}
			}


			return matchedMods;
		}

		private static List<ServerMod> RemoveIgnoredMods(ReadOnlyCollection<ServerMod> serverMods)
		{
			List<ServerMod> validMods = new List<ServerMod>();

			string[] ignoredModsContains = { "extdb", // e.g. @extdb3
			};

			string[] ignoredModsExact = { "arma 3", "addons", "argo", "battleye", "bonus", "curator", "dll",
					"dta", "enoch", "expansion", "heli", "jets", "kart", "keys", "launcher", "mark", "missions",
					"mpmissions", "steamapps", "orange", "tacops", "tank",
			};

			foreach(ServerMod mod in serverMods)
			{
				bool foundInContains = Array.FindAll(ignoredModsContains, str => str.ToLower().Contains(mod.Name.ToLower())).Any();
				if(foundInContains)
					continue;

				bool foundInExact = Array.FindAll(ignoredModsExact, str => str.ToLower().Equals(mod.Name.ToLower())).Any();
				if(foundInExact)
					continue;

				validMods.Add(mod);
			}

			return validMods;
		}

		private static List<string> RemoveIgnoredSignatures(ReadOnlyCollection<string> signatures)
		{
			List<string> validSignatures = new List<string>();

			string[] ignoredSignatures = { "a3"
			};

			foreach(string sig in signatures)
			{
				bool result = ignoredSignatures.Any(s => s.Equals(sig));
				if(result)
					continue;

				validSignatures.Add(sig);
			}

			return validSignatures;
		}
	}
}
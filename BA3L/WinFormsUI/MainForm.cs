﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.IO;

namespace BA3L.WinFormsUI
{
    public partial class MainForm : Form
    {
        private Core core;
		private BindingSource modBindingSource = new BindingSource();
        
        public MainForm(Core core)
        {
            this.core = core;
			StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
			FormClosing += OnFormClosingEvent;

			UpdateParameterValues();
			SetupModList();
			UpdateModList(); // TODO consider putting this in this.Load if it doesn't work
			ModManager.RefreshModsEvent += OnUpdateModList;
			Core.OnGameExitEvent += OnGameExitEvent;

			UpdateSettingValues();
			tlProtonOption.Visible &= Utilities.IsProton(); // Hide Proton executable setting if we aren't using it.

			cbEnableGm.Enabled = DlcManager.GmInstalled;
			cbEnableContact.Enabled = DlcManager.ContactInstalled;
		}

		protected void OnFormClosingEvent(object sender, FormClosingEventArgs e)
		{
			Core.OnGameExitEvent -= OnGameExitEvent;
		}

		private void Play(Core.PlayOptions playOptions)
		{
			try
			{
				core.Play(playOptions);
			}
			catch(Exceptions.ModNotReadyException ex)
			{
				MessageBox.Show("One or more mods are not ready.\nFirst failed mod: " + ex.FailedMod.Name);
			}
			catch(Exceptions.ProtonNotFoundException ex)
			{
				MessageBox.Show("Proton not found: " + ex.Message
					+ "\nSet the path to your Proton executable in Settings.");
			}
		}

		private void BtnPlayWithMods_Click(object sender, EventArgs e)
		{
			Core.PlayOptions playOptions = new Core.PlayOptions();
			playOptions.IncludeLaunchOptions = true;
			playOptions.PlayWithMods = true;
			Play(playOptions);
		}

		private void BtnPlayWithoutMods_Click(object sender, EventArgs e)
		{
			Core.PlayOptions playOptions = new Core.PlayOptions();
			playOptions.IncludeLaunchOptions = true;
			playOptions.PlayWithMods = false;
			Play(playOptions);
		}

		private void SetupModList()
		{
			dgvMods.DataSource = modBindingSource;
			dgvMods.AutoGenerateColumns = false;
			dgvMods.CellContentClick += dgvMods_CellContentClick;
		}

		private void UpdateModList()
		{
			modBindingSource.Clear();

			List<Mod> sortedMods = new List<Mod>(ModManager.GetMods());
			sortedMods.Sort(new ModNameComparer<Mod>());

			foreach(Mod mod in sortedMods)
			{
				modBindingSource.Add(mod);
			}
		}

		private void dgvMods_CellContentClick(object sender, DataGridViewCellEventArgs e)
		{
			// If it was a checkbox and a row was clicked
			if(dgvMods.Columns[e.ColumnIndex] is DataGridViewCheckBoxColumn && e.RowIndex != -1)
			{
				// I don't know if we need to verify that the row in the DataGridView
				// is the same as the one in modBindingSource
				Mod clickedMod = (Mod)modBindingSource[e.RowIndex];
				ModManager.ToggleMod(clickedMod);
				dgvMods.Invalidate();
			}
		}

		private void OnUpdateModList(object sender, RefreshModsEventArgs e)
		{
			Invoke((MethodInvoker)UpdateModList);
		}

		private void OnGameExitEvent(object sender, OnGameExitEventArgs e)
		{
			Console.WriteLine("Game exited.");
		}

		private void UpdateParameterValues()
		{
			cbNoSplash.Checked = Settings.NoSplash;
			cbSkipIntro.Checked = Settings.SkipIntro;
			cbWindowed.Checked = Settings.Windowed;
			txtExtraParams.Text = Settings.ExtraCmdLineParams;
			cbExtraParams.Checked = txtExtraParams.Text.Length > 0;
		}

		private void UpdateSettingValues()
		{
			// Play tab
			cbPlayBattlEye.Checked = Settings.PlayWithBattlEye;
			cbEnableGm.Checked = DlcManager.GmEnabled;
			cbEnableContact.Checked = DlcManager.ContactEnabled;

			// Settings tab
			btnProtonExe.Text = Settings.ProtonPath != "" ? Path.GetFileName(Settings.ProtonPath) : "(none)";
		}

		private void cbNoSplash_CheckedChanged(object sender, EventArgs e)
        {
			Settings.NoSplash = cbNoSplash.Checked;
        }

        private void cbSkipIntro_CheckedChanged(object sender, EventArgs e)
        {
			Settings.SkipIntro = cbSkipIntro.Checked;
        }

        private void cbWindowed_CheckedChanged(object sender, EventArgs e)
        {
			Settings.Windowed = cbWindowed.Checked;
        }

		private void cbPlayBattlEye_CheckedChanged(object sende, EventArgs e)
		{
			Settings.PlayWithBattlEye = cbPlayBattlEye.Checked;
		}

		private const string SaveFileDialogFilter = 
		"BA3L save files (*"  + Settings.EXT_BA3LSAVE + ")|*" 
			+ Settings.EXT_BA3LSAVE + ")|All files (*.*)|*.*";

		private const string LoadFileDialogFilter = 
			"Mod Preset files (*" + Settings.EXT_BA3LSAVE + ";*.html)|*"
			+ Settings.EXT_BA3LSAVE + ";*.html|All files (*.*)|*.*";

		private void BtnSaveModPreset_Click(object sender, EventArgs e)
        {
			using(SaveFileDialog fd = new SaveFileDialog())
			{
				fd.InitialDirectory = Settings.GetLauncherSaveDirectory();
				fd.Filter = SaveFileDialogFilter;
				fd.DefaultExt = Settings.EXT_BA3LSAVE;
				fd.FilterIndex = 1;
				fd.Title = "Save preset";
				fd.RestoreDirectory = true;

				if(fd.ShowDialog() == DialogResult.OK)
				{
					Settings.SaveModPresetFile(fd.FileName, ModManager.GetEnabledMods());
				}
			}

		}

        private void BtnLoadModPreset_Click(object sender, EventArgs e)
        {
			using(OpenFileDialog fd = new OpenFileDialog())
			{
				fd.InitialDirectory = Settings.GetLauncherSaveDirectory();
				fd.Filter = LoadFileDialogFilter;
				fd.DefaultExt = Settings.EXT_BA3LSAVE;
				fd.FilterIndex = 1;
				fd.Title = "Load preset";
				fd.RestoreDirectory = true;

				if(fd.ShowDialog() == DialogResult.OK)
				{
					Settings.LoadModPresetFile(fd.FileName);
					dgvMods.Invalidate();
				}
			}
		}

        private void BtnProtonExe_Click(object sender, EventArgs e)
        {
			using(OpenFileDialog fd = new OpenFileDialog())
			{
				fd.Filter = "Proton executable file (proton)|proton";
				fd.FilterIndex = 1;
				fd.Title = "Select Proton executable";
				fd.RestoreDirectory = true;

				if(fd.ShowDialog() == DialogResult.OK)
				{
					Settings.ProtonPath = fd.FileName;
				}
			}

			UpdateSettingValues();
		}

        private void TxtExtraParams_TextChanged(object sender, EventArgs e)
        {
			cbExtraParams.Checked = txtExtraParams.Text.Length > 0;
			BA3L.Settings.ExtraCmdLineParams = txtExtraParams.Text;
        }

        private void CbEnableGm_CheckedChanged(object sender, EventArgs e)
        {
        	DlcManager.GmEnabled = cbEnableGm.Checked;
		}

        private void CbEnableContact_CheckedChanged(object sender, EventArgs e)
        {
			DlcManager.ContactEnabled = cbEnableContact.Checked;
		}
    }
}
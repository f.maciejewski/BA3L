﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using Gtk;

namespace BA3L.GTK2UI
{
	public partial class ServerInfoWindow : Gtk.Window
	{
		Server server;
		Core core;
		List<MatchedMod> matchedMods = new List<MatchedMod>();

		ListStore requiredModsStore = new ListStore(typeof(MatchedMod));
		ListStore suggestedModsStore = new ListStore(typeof(MatchedMod));
		ListStore notAllowedModsStore = new ListStore(typeof(MatchedMod));
		ListStore actionCellModel = new ListStore(typeof(string));

		public ServerInfoWindow(Core core, Server server, bool startWithDetailsOpen = true) : base(Gtk.WindowType.Toplevel)
		{
			Build();

			expServerDetails.Expanded = startWithDetailsOpen;
			this.server = server;
			this.core = core;
			UpdateServerDetails();

			SetExpanderLabelText();
			UpdateRequiredModsViews();
		}

		private void UpdateServerDetails()
		{
			Title = server.Name;

			lblBattlEyeValue.Text = server.Tags.BattlEye ? "Protected" : "Not protected";
			lblPasswordValue.Text = server.Passworded ? "Has password" : "Without password";
			lblMapValue.Text = server.Map; // TODO translate internal names to human names (e.g. Enoch to Livonia)
			lblMissionValue.Text = server.Mission;
			lblPlayersValue.Text = $"{server.Players}/{server.MaxPlayers}";
			lblServerStateValue.Text = server.Tags.State.ToString();
			lblDifficultyValue.Text = server.Tags.Difficulty.ToString(); // TODO also show difficulty flags from Rules.
			lblPingValue.Text = $"{server.Ping}";
			lblRequiredVersionValue.Text = $"{server.Tags.RequiredVersion}";

			// Server type
			StringBuilder sbServerType = new StringBuilder();
			if(server.Tags.Dedicated)
			{
				sbServerType.Append("Dedicated, ");
			}
			else
			{
				sbServerType.Append("Listen, ");
			}

			if(server.Tags.Platform == Utilities.OS.OS_WINDOWS)
			{
				sbServerType.Append("Windows");
			}
			else
			{
				sbServerType.Append("Linux");
			}

			lblServerTypeValue.Text = sbServerType.ToString();

			// IP address
			lblIpAddressValue.Text = server.Address + ":" + server.ConnectionPort;
		}

		private void UpdateRequiredModsViews()
		{
			if (server.RulesQueryStatus == RulesQueryInfo.Success)
			{
				matchedMods = ModMatchManager.MatchServerMods(server.Rules.ServerMods, server.Rules.Signatures);
				// Create and populate the tree views

				CreateTreeViews();

				bool requiredHasMods = false;
				bool suggestedHasMods = false;
				bool notAllowedHasMods = false;

				foreach(MatchedMod matched in matchedMods)
				{
					switch(matched.MatchType)
					{
						case ModMatchType.Modlist:
							// Add it to "required mods" list
							requiredModsStore.AppendValues(matched);
							requiredHasMods = true;
							break;
						case ModMatchType.Signature:
							// Add it to "suggested + additional mods" list
							suggestedModsStore.AppendValues(matched);
							suggestedHasMods = true;
							break;
						case ModMatchType.NotAllowed:
							// Add it to "not allowed" list
							notAllowedModsStore.AppendValues(matched);
							notAllowedHasMods = true;
							break;
						default:
							// ERROR MESSAGE
							Console.Error.WriteLine($"Unknown mod match type: {matched.MatchType}");
							break;
					}
				}

				if(!requiredHasMods)
				{
					tvModsRequired.Visible = false;
					lblModsRequired.Visible = false;
				}

				if(!suggestedHasMods)
				{
					tvModsSuggested.Visible = false;
					lblModsSuggested.Visible = false;
				}

				if(!notAllowedHasMods)
				{
					tvModsNotAllowed.Visible = false;
					lblModsNotAllowed.Visible = false;
				}


			}
			else
			{
				Console.WriteLine($"Rules query status for {server.Name} is: {server.RulesQueryStatus}");
				// Do something with the mod views
			}
		}

		private void CreateOneView(TreeView tv, ListStore store)
		{
			TreeViewColumn modNameColumn = new TreeViewColumn();
			modNameColumn.Title = "Mod name";
			CellRendererText modNameCell = new CellRendererText();
			modNameColumn.PackStart(modNameCell, true);
			modNameColumn.Expand = true;

			TreeViewColumn actionColumn = new TreeViewColumn();
			actionColumn.Title = "Selected action";
			CellRendererCombo actionCell = new CellRendererCombo();
			actionCell.Editable = true;
			actionCell.HasEntry = false;
			actionCell.Edited += (o, args) => OnServerModActionCellEdited(o, args, store);
			actionCell.Model = actionCellModel;

			// As I understand, this is normally used for the "selected" text of a combobox
			// but we render that text ourselves
			// this property still needs to be set for the combobox to function at all.
			actionCell.TextColumn = 0; 
			actionColumn.PackEnd(actionCell, true);
			actionColumn.Expand = true;

			// Cell data funcs
			modNameColumn.SetCellDataFunc(modNameCell, new TreeCellDataFunc(RenderServerModName));
			actionColumn.SetCellDataFunc(actionCell, new TreeCellDataFunc(RenderServerModAction));

			tv.AppendColumn(modNameColumn);
			tv.AppendColumn(actionColumn);
			tv.HeadersVisible = false;
			tv.Model = store;
			tv.Selection.Changed += OnModSelectionChanged;

		}

		private void CreateTreeViews()
		{
			CreateOneView(tvModsRequired, requiredModsStore);
			CreateOneView(tvModsSuggested, suggestedModsStore);
			CreateOneView(tvModsNotAllowed, notAllowedModsStore);
		}

		private void RenderServerModName(TreeViewColumn tree_column, CellRenderer cell, TreeModel tree_model, TreeIter iter)
		{
			MatchedMod matched = (MatchedMod)tree_model.GetValue(iter, 0);
			if(matched != null)
			{
				(cell as CellRendererText).Text = matched.MatchedModName;
			}
			else
			{
				(cell as CellRendererText).Text = "Unknown mod";
			}
		}

		private void RenderServerModAction(TreeViewColumn tree_column, CellRenderer cell, TreeModel tree_model, TreeIter iter)
		{
			MatchedMod matched = (MatchedMod)tree_model.GetValue(iter, 0);
			if(matched != null)
			{
				(cell as CellRendererCombo).Text = matched.CurrentAction.ToString();
			}
		}

		private void OnModSelectionChanged(object sender, EventArgs e)
		{
			// Sender is of type TreeSelection (aka TreeView.Selection property)
			TreeModel model;
			TreeIter iter;
			TreeSelection selection = (TreeSelection)sender;
			if(selection.GetSelected(out model, out iter))
			{
				// This tree view selected something, deselect the other ones.
				if(selection != tvModsRequired.Selection)
				{
					tvModsRequired.Selection.UnselectAll();
				}
				if(selection != tvModsSuggested.Selection)
				{
					tvModsSuggested.Selection.UnselectAll();
				}
				if(selection != tvModsNotAllowed.Selection)
				{
					tvModsNotAllowed.Selection.UnselectAll();
				}



				// Actually get the selected row and do work
				MatchedMod matched = (MatchedMod)selection.TreeView.Model.GetValue(iter, 0);
				Console.WriteLine($"Selected {matched.MatchedModName}");

				// Clear the ListStore for the shared combobox cell
				// and fill it with the list of actions from the current selection
				// Problems happen if we fill it with the MatchedModAction types directly
				// So we fill it with the ToString() of the action instead.
				actionCellModel.Clear();
				ComboBox box = new ComboBox(actionCellModel);
				box.Clear();

				foreach(var action in matched.Actions)
				{
					box.AppendText(action.ToString());
				}
				box.Active = 0;

			}
			else
			{
				// Likely that the TreeView has deselected.
			}
		}

		private void OnServerModActionCellEdited(object o, EditedArgs args, ListStore treeViewStore)
		{
			CellRendererCombo cell = (CellRendererCombo)o;

			TreeIter iter;
			if(treeViewStore.GetIter(out iter, new TreePath(args.Path)))
			{
				// Get the matched mod corresponding to the cell
				// And set the action from the selected string
				// We had to do it this way because CellRendererCombo doesn't like to use MatchedModAction directly.
				MatchedMod matched = (MatchedMod)treeViewStore.GetValue(iter, 0);
				matched.SetCurrentActionByString(args.NewText);
			}
		}

		protected void OnBtnCloseClicked(object sender, EventArgs e)
		{
			Destroy();
		}

		private void SetExpanderLabelText()
		{
			lblExpandDetails.Text = expServerDetails.Expanded ? "Hide Details" : "Show Details";
		}

		protected void OnExpServerDetailsActivated(object sender, EventArgs e)
		{
			SetExpanderLabelText();
		}

		protected void OnBtnChangeModsAndJoinClicked(object sender, EventArgs e)
		{
			// TODO password prompt?

			// TODO a try/catch for ProtonNotFoundException
			string password;
			if(GetServerPassword(out password))
			{
				core.ApplyModChangesAndJoin(server, password, matchedMods);
				Destroy();
			}
		}

		protected void OnBtnKeepCurrentModsAndJoinClicked(object sender, EventArgs e)
		{
			// TODO password prompt?

			try
			{
				string password;
				if(GetServerPassword(out password))
				{
					core.JoinServer(server, password);
					Destroy();
				}
			}
			catch(Exceptions.ProtonNotFoundException ex)
			{
				string message = "Proton not found: " + ex.Message
	+ "\nSet the path to your Proton executable in Settings.";
				MessageDialog md = new MessageDialog(this, DialogFlags.DestroyWithParent,
					MessageType.Error, ButtonsType.Ok, message);
				md.WindowPosition = WindowPosition.Center;
				md.Run();
				md.Destroy();
			}
		}

		private bool GetServerPassword(out string password)
		{
			password = "";
			if(server.Passworded)
			{
				PasswordDialog passwordDialog = new PasswordDialog(this);
				if(passwordDialog.Run() == (int)ResponseType.Ok)
				{
					// Password got
					password = passwordDialog.Password;
					passwordDialog.Destroy();
					return true;
				}
				else
				{
					// Cancelled/closed
					passwordDialog.Destroy();
					return false;
				}
			}
			else
			{
				return true; // No password so the out string will be "".
			}
		}
	}
}

﻿using System;
using Gtk;

namespace BA3L.GTK2UI
{
	public partial class PasswordDialog : Gtk.Dialog
	{
		public string Password { get; private set; }

		public PasswordDialog(Window parentWindow)
		{
			this.Build();
			Parent = parentWindow;
			DestroyWithParent = true;
		}

		private void OnOk()
		{
			Password = tePasswordBox.Text;
			Respond(ResponseType.Ok);
		}

		private void OnCancel()
		{
			Respond(ResponseType.Cancel);
		}

		protected void OnButtonOkClicked(object sender, EventArgs e)
		{
			OnOk();
		}

		protected void OnButtonCancelClicked(object sender, EventArgs e)
		{
			OnCancel();
		}

		// When you press enter in the text entry.
		protected void OnTePasswordBoxActivated(object sender, EventArgs e)
		{
			OnOk();
		}
	}
}

﻿using System;
using System.Text;
using Steamworks;

namespace BA3L
{
	/// <summary>
	/// Manages optional DLCs: Global Mobilization and Contact
	/// </summary>
	public static class DlcManager
	{
		public const int APPID_GM = 1042220; // Global Mobilization: Cold War Germany
		public const int APPID_CONTACT = 1021790; // Arma 3: Contact

		private static bool _GmEnabled;
		public static bool GmEnabled
		{
			get { return _GmEnabled; }

			set
			{
				if(value)
				{
					if(GmInstalled)
					{
						_GmEnabled = true;
					}
				}
				else
				{
					_GmEnabled = false;
				}
			}
		}

		private static bool _ContactEnabled;
		public static bool ContactEnabled
		{ 
			get { return _ContactEnabled; }
			set
			{
				if(value)
				{
					if(ContactInstalled)
					{
						_ContactEnabled = true;
					}
				}
				else
				{
					_GmEnabled = false;
				}
			}
		}

		public static bool GmInstalled => SteamApps.IsDlcInstalled(APPID_GM);
		public static bool ContactInstalled => SteamApps.IsDlcInstalled(APPID_CONTACT);

		public static string GenerateDlcLaunchParameters()
		{
			StringBuilder sb = new StringBuilder();

			if(GmEnabled)
			{
				sb.Append("-mod=GM ");
			}

			if(ContactEnabled)
			{
				sb.Append("-mod=Contact ");
			}

			return sb.ToString();
		}
	}
}

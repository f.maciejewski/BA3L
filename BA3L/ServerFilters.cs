﻿using System.Text;

namespace BA3L
{
	public enum BattlEyeFilter
	{
		Any,
		Protected,
		NotProtected,
	}

	public static class ServerFilters
	{
		// https://community.bistudio.com/wiki/STEAMWORKSquery#Table

		public static BattlEyeFilter BattlEye = BattlEyeFilter.Any;
		public static bool NoPasswordRequired = false;
		public static bool NotFull;
		public static bool HasPlayers;



		public static void AddFilters(Steamworks.ServerList.Base serverQuery)
		{
			// https://partner.steamgames.com/doc/api/ISteamMatchmakingServers#MatchMakingKeyValuePair_t
			// gametagsand = Server contains all of the specified strings in a comma-delimited list.

			if(BattlEye == BattlEyeFilter.Protected)
			{
				serverQuery.AddFilter("gametagsand", "bt");
			}
			else if(BattlEye == BattlEyeFilter.NotProtected)
			{
				serverQuery.AddFilter("gametagsand", "bf");
			}

			// As far as I know, we can't filter by password here; do this in PassesFilter.

			// Players filters
			if(NotFull)
			{
				serverQuery.AddFilter("notfull", ""); // no value needed
			}

			if(HasPlayers)
			{
				serverQuery.AddFilter("hasplayers", ""); // no value needed
			}


		}

		public static bool PassesFilter(Server server)
		{
			if(BattlEye != BattlEyeFilter.Any)
			{
				// Protected servers only. Remove non-protected servers.
				if(BattlEye == BattlEyeFilter.Protected && !server.Tags.BattlEye)
					return false;

				// Non-protected servers only. Get rid of BattlEye-protected servers
				if(BattlEye == BattlEyeFilter.NotProtected && server.Tags.BattlEye)
					return false;
			}


			// Non-passworded servers only
			if(NoPasswordRequired && server.Passworded)
				return false;

			if(NotFull && (server.Players >= server.MaxPlayers)) // in case a server says it has 11/10 players for secret admin slots
				return false;

			if(HasPlayers && server.Players == 0)
				return false;




			return true;
		}




	}
}

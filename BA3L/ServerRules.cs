﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;

namespace BA3L
{
	// https://community.bistudio.com/wiki/Arma_3_ServerBrowserProtocol3#DLC_Flags
	[Flags]
	public enum Expansions : int
	{
		Kart = 1,
		Marksmen = 2,
		Heli = 4,
		Curator = 8,
		Expansion = 16,
		Jets = 32,
		Orange = 64,
		Argo = 128,
		TacOps = 256,
		Tanks = 512,
		Contact = 1024,
		Enoch = 2048,
		// TODO future-proof DLC bytes by adding e.g. Unknown1 = 4096?
	}

	public struct ServerMod
	{
		public uint Hash;
		public ulong WorkshopId;
		public string Name;
	}

	public class ServerRules
	{
		public int Version { get; private set; } // Rules query version, not server version
		public bool Overflow { get; private set; }
		public Expansions Expansions { get; private set; }
		public int ExpansionCount { get; private set; }

		//List<ServerMod> ServerMods;
		public ReadOnlyCollection<ServerMod> ServerMods { get; private set; }
		public ReadOnlyCollection<string> Signatures { get; private set; }


		public ServerRules(Dictionary<byte[], byte[]> rulesDictionary)
		{
			ProcessRules(rulesDictionary);
		}

		/// <summary>
		/// Processes the server rules as explained in the documentation
		/// https://community.bistudio.com/wiki/Arma_3_ServerBrowserProtocol3
		/// </summary>
		/// <param name="rulesDictionary">Rules dictionary.</param>
		private void ProcessRules(Dictionary<byte[], byte[]> rulesDictionary)
		{
			// Reassemble the actual server message
			List<byte> bytes = new List<byte>();
			foreach(KeyValuePair<byte[], byte[]> pair in rulesDictionary)
			{
				// Ignore the key, since it isn't part of the payload.
				bytes.AddRange(pair.Value);
			}

			byte[] fullBytes = bytes.ToArray();

			// Restore escaped bytes
			byte[] restoredBytes = RestoreBytes(fullBytes);

			using(BinaryReader br = new BinaryReader(new MemoryStream(restoredBytes)))
			{
				Version = br.ReadByte();
				if(Version != 3)
				{
					throw new Exceptions.InvalidRulesException($"Rules version {Version} not supported!");
				}

				// TODO separate the two flags, and figure out their significance.
				// One has to be Mod list overflow, the other has to be Signature List overflow
				int overflowBytes = br.ReadByte();
				// Just report any kind of overflow flag until we can separate them.
				Overflow = overflowBytes > 0;

				ushort dlcBytes = br.ReadUInt16();
				// Expansions = (Expansions)BitConverter.ToInt16(byteStream, 2);
				Expansions = (Expansions)dlcBytes;
				ExpansionCount = Expansions.ToString().Split(',').Length;

				int difficultyFlags = br.ReadByte(); // TODO decode these flags.
				int crosshairByte = br.ReadByte(); // TODO Do this when decoding difficulty flags

				if(crosshairByte != 0 && crosshairByte != 1)
				{
					throw new Exceptions.InvalidRulesException($"Crosshair byte has invalid value: {crosshairByte}");
				}

				// DLC Hashes for each enabled DLC/Expansion
				for(int i = 0; i < ExpansionCount; i++)
				{
					// Not sure if we need to care about them, so just skip through them.
					br.ReadInt32(); // 4-byte DLC hashes
				}

				// Mods
				int modCount = br.ReadByte();
				List<ServerMod> serverMods = new List<ServerMod>();
				for(int i = 0; i < modCount; i++)
				{
					ServerMod serverMod;
					// Mod hash (4 bytes)
					// Not sure if we should be doing anything with this hash yet.
					serverMod.Hash = br.ReadUInt32();

					// Mod steam ID length (4 bits) + isDlc flag (1 bit)
					int modSteamIdLength = br.ReadByte();
					// Separate the isDlc flag from the steam id length
					int dlcBit = 5 % 8; // 5th bit
					byte isDlcMask = (byte)(1 << dlcBit - 1); // 5th bit is 4 because 0-based index
					bool isDlc = (modSteamIdLength & isDlcMask) != 0;
					modSteamIdLength &= ~isDlcMask;

					byte[] steamIdBytes = br.ReadBytes(modSteamIdLength);
					serverMod.WorkshopId = 0;
					// Non-workshop mods have a workshop ID length of 1, and an ID of 0.
					if(steamIdBytes[0] != 0)
					{
						serverMod.WorkshopId = BitConverter.ToUInt32(steamIdBytes, 0);
					}

					serverMod.Name = br.ReadLengthPrefixedString();

					serverMods.Add(serverMod);
				}

				ServerMods = new ReadOnlyCollection<ServerMod>(serverMods);

				// Signatures
				int signatureCount = br.ReadByte();
				List<string> sigs = new List<string>();
				for(int i = 0; i < signatureCount; i++)
				{
					sigs.Add(br.ReadLengthPrefixedString());
				}

				Signatures = new ReadOnlyCollection<string>(sigs);
			}
		}

		/// <summary>
		/// Restores escaped byte sequences that couldn't be transferred through the rules query.
		/// </summary>
		/// <returns>A byte array containing the true byte sequence of the message</returns>
		/// <param name="oldBytes">Escaped byte array</param>
		private static byte[] RestoreBytes(byte[] oldBytes)
		{
			List<byte> newBytes = new List<byte>();

			// Using for loop instead of foreach because we need to look ahead
			for(int i = 0; i < oldBytes.Length; i++)
			{
				if(oldBytes[i] == 0x01)
				{
					switch(oldBytes[i + 1])
					{
						case 0x01:
							// Restore 0x01 0x01 to 0x01
							newBytes.Add(0x01);
							i++;
							break;
						case 0x02:
							// Restore 0x01 0x02 to 0x00
							newBytes.Add(0x00);
							i++;
							break;
						case 0x03:
							// Restore 0x01 0x03 to 0xFF
							newBytes.Add(0xFF);
							i++;
							break;
						default:
							newBytes.Add(oldBytes[i]);
							break;
					}

				}
				else
				{
					newBytes.Add(oldBytes[i]);
				}
			}

			return newBytes.ToArray();
		}
	}
}

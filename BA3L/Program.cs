﻿using System;
using System.Collections.Generic;
using BA3L.WinFormsUI;

namespace BA3L
{
	public static class MainClass
	{
		public static void Main(string[] args)
		{
			InitSteamworks();

			// Handle ours + official launcher's command line arguments
			// https://community.bistudio.com/wiki/Arma_3_Launcher#Command_line_parameters
			List<string> gameArgs = new List<string>();

			Dictionary<string, bool> launcherArgs = new Dictionary<string, bool>
			{
				{"-safemode",   false},  // Unused in BA3L
				{"-nohwa",      false},  // Unused in BA3L
				{"-nolauncher", false},  // Skips launcher and starts Arma 3 directly. Ignores -console
				{"-usebe",      false},  // Used with -noLauncher, starts BattlEye. Ignores -win32 and -win64 (Windows-only)
				{"-win32",      false},  // Used with -noLauncher, starts 32-bit version of game (Windows-only)
				{"-win64",      false},  // Used with -noLauncher, starts 64-bit version of game (Windows-only)
				{"-console",    false},  // BA3L: Use a console GUI
				{"-winforms",   false},  // BA3L: Use a WinForms GUI
				{"-gtk",        false},  // BA3L: Use a GTK GUI
				{"-restarted",  false}   // BA3L: Check if we have restarted the launcher automatically. Don't use manually.
			};

			// Go through each argument, using it or passing it to gameArgs.
			foreach (string arg in args)
			{
				if (launcherArgs.ContainsKey(arg.ToLower()))
				{
					launcherArgs[arg.ToLower()] = true;
				}
				else
				{
					gameArgs.Add(arg);
				}
			}

			Core core = new Core(gameArgs, launcherArgs); // TODO Consider a "simple core" for -noLauncher so it doesn't load everything we don't need

			if(launcherArgs["-nolauncher"])
			{
				StartNoLauncher(launcherArgs, core);
			}
			else 
			{
				if(launcherArgs["-console"])
				{
					StartConsoleUI(args, core);
				}
				else if(launcherArgs["-winforms"])
				{
					StartWinFormsUI(core);
				}
				else if(launcherArgs["-gtk"])
				{
					StartGTK2UI(core);
				}
				else
				{
					StartDefaultUI(core);
				}
			}


			// UIs are closed, program is ending
			core.Shutdown();
			Steamworks.SteamClient.Shutdown();
		}

		private static void StartNoLauncher(Dictionary<string, bool> launcherArgs, Core core)
		{
			Console.WriteLine("Starting Arma 3 with no launcher");
			// Launch Arma 3 with no more input, and then exit.
			// -win32 and -win64 are only used together with -noLauncher

			Core.PlayOptions playOptions = new Core.PlayOptions();
			playOptions.Force32Bit = launcherArgs["-win32"];
			playOptions.Force64Bit = launcherArgs["-win64"];
			playOptions.ForceBattlEye = launcherArgs["-usebe"];
			playOptions.IncludeLaunchOptions = false;
			playOptions.PlayWithMods = false;
			core.Play(playOptions);
		}

		/// <summary>
		/// Starts the default user interface for the user's OS. Windows will use WinForms, Linux will use GTK.
		/// </summary>
		/// <param name="core">Core</param>
		private static void StartDefaultUI(Core core)
		{
			if(Utilities.GetOS() == Utilities.OS.OS_WINDOWS)
			{
				StartWinFormsUI(core);
			}
			else
			{
				StartGTK2UI(core);
			}
		}

		private static void StartWinFormsUI(Core core)
		{
			Console.WriteLine("Starting BA3L WinForms UI");
			WinFormsLauncher.Run(core);
		}

		private static void StartGTK2UI(Core core)
		{
			Console.WriteLine("Starting BA3L GTK2 UI");
			// Use GTK2 UI
			try
			{
				GTK2UILauncher gtk2UI = new GTK2UILauncher(core);
				gtk2UI.Run();
			}
			catch (System.IO.FileNotFoundException e)
			{
				if (e.Message.Contains("gtk-sharp"))
				{
					Console.Error.WriteLine("Error: gtk-sharp not found.");
					Console.Error.WriteLine(e);
				}
				else
				{
					throw;
				}
			}
		}

		private static void StartConsoleUI(string[] args, Core core)
		{
			Console.WriteLine("Starting BA3L Console UI");
			// Use console UI
			ConsoleUI con = new ConsoleUI(core);
			bool result = con.Run();
			if (!result)
			{
				// Restart with a console window
				// Assume this is a Windows/Wine-specific and won't trigger on OSX/Linux
				string argsString = Utilities.ArgsToString(args);
				System.Diagnostics.Process.Start("cmd.exe", "/c arma3launcher.exe " + argsString + "-restarted");
			}
		}

		private static void InitSteamworks()
		{
			try
			{
				Steamworks.SteamClient.Init(Utilities.ARMA3_APPID);
			}
			catch(Exception e)
			{
				Console.Error.WriteLine("Unable to initialize Steamworks!");
				Console.Error.WriteLine("Please ensure Steam is running.");
				Console.Error.WriteLine(e);
			}

			Console.WriteLine("Steam Api is valid: " + Steamworks.SteamClient.IsValid);
		}

	}
}

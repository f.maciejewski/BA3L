﻿using System;
using System.Diagnostics;
using System.IO;

namespace BA3LLauncher
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			Console.WriteLine("BA3L Launcher");
			string BA3LPath = Path.Combine(Directory.GetCurrentDirectory(), "BA3L", "BA3L.exe");
			ProcessStartInfo startInfo = new ProcessStartInfo();
			startInfo.WorkingDirectory = Path.Combine(Directory.GetCurrentDirectory(), "BA3L");
			startInfo.Arguments = ArgsToString(args);
			startInfo.FileName = BA3LPath;
			startInfo.UseShellExecute = false; // Work around Console.WriteLine crash
			Process p = Process.Start(startInfo);
			p?.WaitForExit(); // Work around Console.WriteLine crash
			Console.WriteLine("BA3L has exited.");
		}

		public static string ArgsToString(string[] args)
		{
			string argsString = "";

			foreach (string arg in args)
			{
				argsString += arg + " ";
			}

			return argsString;
		}
	}
}
